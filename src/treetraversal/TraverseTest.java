package treetraversal;


public class TraverseTest {

	public static void main(String[] args) {
		Node Cnode = new Node("C", null, null);
		Node Enode = new Node("E", null, null);
		Node Dnode = new Node("D", Cnode, Enode);
		Node Anode = new Node("A", null, null);
		Node Bnode = new Node("B", Anode, Dnode);
		Node Hnode = new Node("H", null, null);
		Node Inode = new Node("I", Hnode, null);
		Node Gnode = new Node("G", null, Inode);
		Node Fnode = new Node("F", Bnode, Gnode);

		ReportConsole report = new ReportConsole();
		
		report.display(Fnode, new PreOrderTraversal());
		report.display(Fnode, new InOrderTraversal());
		report.display(Fnode, new PostOrderTraversal());


	}

}
