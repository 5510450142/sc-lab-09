package treetraversal;

import java.util.List;


public class ReportConsole {

	public void display(Node root, Traversal traversal) {
		
		List<Node> listwalk = traversal.traverse(root);
		String name = traversal.getClass().getSimpleName();
		System.out.print("Traverse with "+name+": ");
		for (Node nodes : listwalk) {
			System.out.print(nodes.getValue() + " ");
		}
		System.out.println();
	}

}
