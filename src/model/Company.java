package model;




public class Company implements Taxable{
	private String name;
	private double receive;
	private double expenses;
	
	public Company(String name, double receive, double expenses){
		this.name = name;
		this.receive = receive;
		this.expenses = expenses;
	}
	
	public String getName(){
		return this.name;
	}
	
	public double getReceive(){
		return this.receive;
	}
	
	public double getExpenses(){
		return this.expenses;
	}

	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double result = 0;
		result = (this.getReceive() - this.getExpenses())*0.30;
		return result;
	}
	
	public double getProfit(){
		double result = 0;
		result = receive - expenses;
		return result;
		
	}
	
	public String toString(){
		return this.name + " " + this.receive + " " + this.expenses ;
		
	}
}
