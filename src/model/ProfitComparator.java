package model;

import java.util.Comparator;

public class ProfitComparator implements Comparator<Company>{
	
	@Override
	public int compare(Company com1, Company com2) {
		// TODO Auto-generated method stub
		if(com1.getProfit() < com2.getProfit())
			return -1;
		else if(com1.getProfit() > com2.getProfit())
			return 1;
		else
			return 0;
	}

}
