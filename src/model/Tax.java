package model;

import java.util.Comparator;

public class Tax implements Taxable,Comparator<Tax>{

	@Override
	public int compare(Tax tax1, Tax tax2) {
		// TODO Auto-generated method stub
		if(tax1.getTax() < tax2.getTax())
			return -1;
		else if(tax1.getTax() > tax2.getTax())
			return 1;
		else
			return 0;
	}

	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		return 0;
	}
}
