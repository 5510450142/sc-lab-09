package model;


public class Product implements Taxable,Comparable<Product>{
	private String nameProduct;
	private double price;
	
	public Product(String nameProduct, double price){
		this.nameProduct = nameProduct;
		this.price = price;
	}
	
	public String getName(){
		return this.nameProduct;
	}
	
	public double getPrice(){
		return this.price;
	}

	public double getTax() {
		// TODO Auto-generated method stub
		return this.getPrice()*0.07;
	}

	@Override
	public int compareTo(Product obj) {
		// TODO Auto-generated method stub
		Product other = (Product) obj;
		if(this.getPrice() < other.getPrice())
			return -1;
		else if(this.getPrice() > other.getPrice())
			return 1;
		else
			return 0;
	}
	
	public String toString(){
		return this.nameProduct + " " + this.price ;	
	}
}
