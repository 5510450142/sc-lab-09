package model;

import java.util.ArrayList;


public class TaxCalculator {
	
	public static double result(ArrayList<Taxable> taxList){
		double result = 0;
		for (Taxable obj : taxList) {
			result = result + obj.getTax(); 
		}
		return result;
	}
	
}
